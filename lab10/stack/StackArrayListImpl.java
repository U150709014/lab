<<<<<<< HEAD
package stack;

import java.util.ArrayList;
import java.util.List;

public class StackArrayListImpl<T> implements Stack<T> {

	ArrayList<T> stack = new ArrayList<T>();
	
	@Override
	public void push(T item) {
		stack.add(0, item);
	}

	@Override
	public T pop() {		
		return stack.remove(0);
	}

	@Override
	public boolean empty() {
		return stack.isEmpty();
	}

	@Override
	public List<T> toList() {
		
		return stack;
	}

	@Override
	public void addAll(Stack<T> aStack) {
		stack.addAll(0, aStack.toList());
		
	}
}

=======
package stack;

import java.util.ArrayList;
import java.util.List;

public class StackArrayListImpl<T> implements Stack<T> {

	ArrayList<T> stack = new ArrayList<T>();
	
	@Override
	public void push(T item) {
		stack.add(0, item);
	}

	@Override
	public T pop() {		
		return stack.remove(0);
	}

	@Override
	public boolean empty() {
		return stack.isEmpty();
	}

	@Override
	public List<T> toList() {
		
		return stack;
	}

	@Override
	public void addAll(Stack<? extends T> aStack) {
		stack.addAll(0, aStack.toList());
		
	}
}
>>>>>>> remotes/OzgurKilic/lab/master
