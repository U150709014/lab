package zeynep.shapes3d;

import zeynep.shapes.Circle;

public class Cylinder extends Circle {
	
	private int height;
	public Cylinder(int r,int h) {
		super(r);
		height = h;
	}

	
	public double area(){
		return 2 * super.area() +height * 2 * Math.PI * getRadius();
	}
	

	public double volume(){
		return super.area() * height;
	}
	
	public String toString(){
		return "radius: " + getRadius() + "height: " + height;
	}
	
}

