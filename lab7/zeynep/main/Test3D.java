package zeynep.main;


	import zeynep.shapes3d.*;
	import zeynep.shapes.*;

	public class Test3D {

		public static void main(String[] args) {
			Cylinder cylinder = new Cylinder(5,6);
			Circle circ = new Circle(5);
			
			System.out.println(circ.area());
			
			System.out.println(cylinder.area());
			System.out.println(cylinder.volume());
			
			Circle circle = cylinder;
			
			System.out.println(circle.area());
			
			Box box = new Box(5,6,7);
			
			System.out.println();
			System.out.println(box.area());
			System.out.println(box.volume());
		}
	}

