package drawing;

public class Circle {
	
	private int radius;
	private Point center;
	
	public Circle(int radius, Point  center){
		this.radius = radius;
		this.center = center;
	}

	public void drawCircle(){
		System.out.println("Drawing Circle at " + center + " having radius " + radius);
	}
	
	public double areaOfCircle(){
		return Math.PI * radius * radius;
	}
	
	public void moveCircle(int xDistance,  int yDistance){
		center.move(xDistance, yDistance);
	}
}
<<<<<<< HEAD

=======
>>>>>>> remotes/OzgurKilic/lab/master
