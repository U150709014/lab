package drawing;

import java.util.ArrayList;

public class Drawing {
	
	private ArrayList<Circle> circles = new ArrayList<>();
	private ArrayList<Rectangle> rectangles = new ArrayList<>();
	
	public double totalArea(){
		double totalArea = 0;
		
		for(Circle circle: circles){
			totalArea += circle.areaOfCircle();
		}

		for(Rectangle rect: rectangles){
			totalArea += rect.areaOfRectangle();
		}
		
		return totalArea;
	}
	
	
	public void draw(){
		for(Circle circle: circles){
			circle.drawCircle();
		}

		for(Rectangle rect: rectangles){
			rect.drawRectangle();
		}
		
	}

	
	public void move(int xDistance,  int yDistance){
		for(Circle circle: circles){
			circle.moveCircle(xDistance, yDistance);
		}

		for(Rectangle rect: rectangles){
			rect.moveRectangle(xDistance, yDistance);
		}
		
	}
	
	public void addCircle(Circle circle){
		circles.add(circle);
	}
	
	public void addRectangle(Rectangle rect){
		rectangles.add(rect);
	}
	
}
<<<<<<< HEAD

=======
>>>>>>> remotes/OzgurKilic/lab/master
