package midterm;

public class Duck extends Animal {

	public Duck(String name) {
		super(name);
	}

	
	public String speak() {
		return "Quack";
	}

}
